plz <- read_sf("https://fbinter.stadt-berlin.de/fb/wfs/data/senstadt/s_plz?service=wfs&version=2.0.0&request=GetFeature&TYPENAMES=s_plz")
plz$plz_r <- as.character(trunc(plz$plz / 1000))
plz <- plz[plz$plz_r %in% 10:14,]
jpeg("2022-03-02_plz.jpg",
     width=1200, height=1000, quality=90,
     res=120)
plz %>%
  group_by(plz_r) %>%
  summarise() %>%
  ggplot() +
  geom_sf(aes(fill=plz_r)) +
  geom_sf_text(aes(label = plz_r))+
  scale_fill_brewer(palette ="Set3") +
  theme_void()
dev.off()
