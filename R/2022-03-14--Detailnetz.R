library(sf)
library(dplyr)
library(ggplot2)

# "https://fbinter.stadt-berlin.de/fb/wfs/data/senstadt/s_vms_detailnetz_spatial_gesamt"
# https://tsb-opendata.s3.eu-central-1.amazonaws.com/detailnetz_strassenabschnitte/Detailnetz-Strassenabschnitte.gml
dsn <- read_sf("raw_data/Detailnetz-Strassenabschnitte.gml")
lor <- st_read("raw_data/lor_planungsraeume_2021.gml")
colnames(lor)[1:6] |> tolower() ->  colnames(lor)[1:6]
lor$plr_id <- lor$plr_id |> as.factor()
subset(lor, select=-bez) -> lor
lor_outer <- st_as_sf(st_union(lor))


ggplot(dsn) +
  geom_sf(data=lor_outer) +
  geom_sf(lwd=0.5) +
  theme_void() +
  theme(legend.key.width = unit(0.1, "npc"),
        legend.position="bottom") +
  labs(title = "Road network",
       subtitle = "Main axes in Berlin",
            caption = "Data: Detailnetz - Straßenabschnitte \nCC BY Geoportal Berlin")
ggsave("2022-03-14_detailnetz.jpg", width=7.0,
       height=6, bg="white", dpi = 160)

