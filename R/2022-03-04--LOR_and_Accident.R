library(sf)
library(dplyr)
library(ggplot2)
crash <-
  read.csv2("raw_data/AfSBBB_BE_LOR_Strasse_Strassenverkehrsunfaelle_2020_Datensatz.csv",
colClasses = c(rep("character", 1),
               rep("factor", 12),
               rep("integer", 6),
               rep("factor", 1),
               rep("numeric", 4)
))

crash[14:19] <- apply( crash[14:19], 2, as.logical)

crash |>
 group_by(LOR_ab_2021, BEZ) |>
 summarise(count = n()) -> crash_lored


jpeg("2022-03-04_lor_crashed.jpg",
     width=1200, height=1000, quality=90,
     res=120)
  ggplot(crash_lored, aes(count)) +
  geom_histogram(aes(fill=BEZ), position="stack", binwidth=10)+
  scale_fill_viridis_d(option = "A") +
  theme_minimal()+
  xlab("Number of accidents by classes of 10 accidents")+
  ylab("Number of LOR")
dev.off()
