---
layout: post
title: Grundbegriffe
---

German authorities record road accident with number as key for
factor value. For example, in many cases 0 means FALSE and 1
means TRUE. To understand the statistic, the Federal Statistical
Office (Statistisches Bundesamt − Destatis) publishes a
[booklet explaining][GdbVkhflstat] the basic concepts of the
database about traffic accident. Today, I was curious to look at the
variable accident class (Unfallart), describing the direction of
movement of the vehicles involved.  Accidents are categorised
into 10 classes:

 1. Collision with another vehicle that starts, stops or is in stationary traffic

 2. Collision with another vehicle driving ahead or waiting

 3. Collision with another vehicle driving sideways in the same direction.

 4. Collision with another vehicle traveling in the opposite direction.

 5. Collision with another vehicle turning or crossing.

 6. Collision between vehicle and pedestrian.

 7. Impact on an obstacle on the road.

 8. Leaving the lane to the right.

 9. Leaving the lane to the left.

 10. Accidents of a different class.

In your opinion, what is the main class of accident involving
bikes?

[GdbVkhflstat]: https://www.destatis.de/DE/Themen/Gesellschaft-Umwelt/Verkehrsunfaelle/Methoden/_inhalt.html#sprg371798

## Picture of the day

A graph of the accidents involving a bike in
Berlin and Brandenburg split by class.

<!--
library(sf)

# Select Berlin and Brandenburg
# ULAND = 11 (Berlin) | 12 (Brandenburg)
berbrand <- read.csv2(pipe("awk 'BEGIN {FS=\";\"} {if ($3 == 11 || $3 == 12 ) print $0}' Unfallorte2020_LinRef.csv"),
header=FALSE,
colClasses = c(rep("character", 2),
               rep("factor", 18),
               rep("character", 4),
               rep("factor", 1)
))
readLines("Unfallorte2020_LinRef.csv", n=1) |>
  strsplit(";") |> unlist()  |> tolower() -> colnames(berbrand)
colnames(berbrand)
levels(berbrand$uwochentag) <- c("So", "Mo", "Di", "Mi", "Do",
                                 "Fr", "Sa")
levels(berbrand$ulichtverh) <-
  c("Tageslicht","Dämmerung","Dunkelheit")
berbrand$uwochentag <-
  factor(berbrand$uwochentag,levels(berbrand$uwochentag)[c(2:7,1)])

# Select only when bike are involved
 berbrand <- berbrand[berbrand$istrad == 1,]

# Colour palette
retro <- c( "#9239F6", "#903495"  , "#6F3460"  , "#4A354F"  ,
"#D20076"  , "#FF0076"  , "#FF4373"  , "#FF6B58", "#F8B660",
"red")

library(ggplot2)
library(dplyr)

# Bike accident by class
jpeg("bike_unfaelle_class.jpg", width=800, quality=90)
ggplot(berbrand, aes(x = uart)) +
  geom_bar(aes(fill = uart)) +
    theme_minimal() +
    guides(fill="none") +
                      scale_fill_manual( values = retro ) +
 labs(x = "Type")
dev.off()

-->
<figcaption>
Exhibit made with R (code provided in a comment of this webpage)
Data Copyright: DL-DE BY 2.0 Statistische Ämter des Bundes und
der Länder, Deutschland, 2021.
</figcaption>

![Number of bike accident by class][2022-02-24_bike_unfaelle_class]

[2022-02-24_bike_unfaelle_class]: {{site.canonical}}/assets/exhibit/2022-02-24_bike_unfaelle_class.jpg

The answer is unambiguous: 49.5 % of the accidents
are of class 5 (i.e. when turning, and I suspect that this is mostly
the case when cars are turning on the right and did not check
if a bike was already there). Then we have the case [1]0 (almost
20%) and the case 1 (8%), but I am not sure what that
concretely means and I will have to look closer tomorrow.

