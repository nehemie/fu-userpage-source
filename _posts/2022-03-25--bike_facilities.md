---
layout: post
title: Bike facilities
...

Let us bring new data. Another [dataset][dataset-facilities] provides information
about the bike facilities to ride in the city. It includes 
 - bike lane (*Radweg*)
 - shared sidewalks (*gemeinsamer Geh- und Radweg*)
 - separated sidewalks with bike lanes (*getrennter Geh- und Radweg*),
 - sidewalks with additional sign bike traffic free (*Radverkehr frei*).
 - cycle lanes (*Radfahrstreifen*)
 - bus lanes free for cyclists (*Bussonderfahrstreifen*)

## Exhibit of the day

A crude map of bike facilities over the road network

![map bike facilities][2022-03-25_bike_facilities]

[2022-03-25_bike_facilities]: {{site.canonical}}/assets/exhibit/2022-03-25_bike_facilities.jpg


<figcaption> Data:Radverkehrsanlagen   + Detailnetz Straßenabschnitte CC BY Geoportal Berlin".</figcaption>

Plot made with [R], [sf] and [ggplot2] (code in the source page as comments).

[R]: {{ site.data.links.cran }}
[sf]: {{ site.data.links.cran_sf }}
[ggplot2]: {{ site.data.links.cran_ggplot2 }}
[data]: https://daten.berlin.de/datensaetze/detailnetz-stra%C3%9Fenabschnitte-wfs
[dataset-facilities]: https://fbinter.stadt-berlin.de/fb_daten/beschreibung/radverkehrsanlagen.html
[yesterday]: 2022-03-24-bike-accident-weekday
[today]: 2022-03-25-bike-facilities


<!--

library(sf)
library(dplyr)
library(ggplot2)
library(units)

# Load data road network
# "https://fbinter.stadt-berlin.de/fb/wfs/data/senstadt/s_vms_radverkehrsanlagen_rva?service=wfs&version=2.0.0&request=GetFeature&TYPENAMES=s_vms_radverkehrsanlagen_rva"

rva <- read_sf("raw_data/s_vms_radverkehrsanlagen_rva.gml")
rva <- rva[,4:11]
rva <- st_set_crs(rva, 25833) #ETRS89 / UTM zone 33N


dsn <- read_sf("raw_data/Detailnetz-Strassenabschnitte.shp")

# Berlin outline
lor <- st_read("raw_data/lor_planungsraeume_2021.gml")
colnames(lor)[1:6] |> tolower() ->  colnames(lor)[1:6]
lor$plr_id <- lor$plr_id |> as.factor()
subset(lor, select=-bez) -> lor
lor_outer <- st_as_sf(st_union(lor))

ggplot(rva) +
  geom_sf(data = lor_outer,
          fill = "black") +
  geom_sf(data = dsn,
          col = "white",
          alpha = 0.5) +
  geom_sf(data = rva,
           col = "red",
          size = 1.2,
         alpha = 0.9) +
  theme_void() +
  labs(title = "Bike facilities",
       subtitle = "Berlin / 2020",
      caption = "Data: Radverkehrsanlagen + Detailnetz - Straßenabschnitte + Detailnetz - Straßenabschnitte \nCC BY Amt für Statistik Berlin-Brandenburg & Geoportal Berlin")

ggsave("2022-03-25_bike_facilities.jpg",
       width = 7.0,
       height = 6,
       bg = "white",
       dpi = 220)

-->
