---
layout: post
title: Brötchen
---

“Brötchen” are something that I love. Last weekend, I
tried to make my own variation, a sourdough bread roll with an
even mix of whole rye and wheat floor.

## Picture of the day

Frische, knusprige, ofenfrische, selbst-gebackene Brötchen

<figcaption> CC BY-SA 4.0 Néhémie Strupler (2022-02-13)</figcaption>

![Home-made Sourdough German-style bread roll][2022-02-17_broetchen]


[2022-02-17_broetchen]: {{site.canonical}}/assets/exhibit/2022-02-17_broetchen.jpg

