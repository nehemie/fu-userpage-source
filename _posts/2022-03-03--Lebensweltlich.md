---
layout: post
title: Lebensweltlich
...


"Lebensweltlich orientierten Räume" (LOR), also something roughly
translated as  "lifeworld-oriented spaces" are the smaller spatial units
for spatial planning and official statistics in Berlin. These
units aim to grasp a common living environment while at the same time
maintaining the comparability between them.

Used since 2006, there were modified in
[2020](https://www.stadtentwicklung.berlin.de/planen/basisdaten_stadtentwicklung/lor/),
and the new organisation is implemented since 2021.

## Picture of the day

A Map made with [r], [sf] and [ggplot2] of Berlin showing the LOR
units coloured by [district (borough)](2022-03-01-bezirke) since
2021.

<figcaption>
Data: Lebensweltlich orientierte Räume (LOR) in Berlinn, CC-BY-3.0 Amt für Statistik Berlin-Brandenburg
</figcaption>

![Belin's LOR][2022-03-03-LOR]

[2022-03-03-LOR]: {{site.canonical}}/assets/exhibit/2022-03-03_lor.jpg

[R]: {{ site.data.links.cran }}
[sf]: {{ site.data.links.cran_sf }}
[ggplot2]: {{ site.data.links.cran_ggplot2 }}
[data]: https://fbinter.stadt-berlin.de/fb/?loginkey=alphaDataStart&alphaDataId=s_plz@senstadt

<!--

plz <- read_sf("https://fbinter.stadt-berlin.de/fb/wfs/data/senstadt/s_plz?service=wfs&version=2.0.0&request=GetFeature&TYPENAMES=s_plz")
plz$plz_r <- as.character(trunc(plz$plz / 1000))
plz <- plz[plz$plz_r %in% 10:14,]
jpeg("2022-03-02_plz.jpg",
     width=1200, height=1000, quality=90,
     res=120)
plz %>%
  group_by(plz_r) %>%
  summarise() %>%
  ggplot() +
  geom_sf(aes(fill=plz_r)) +
  geom_sf_text(aes(label = plz_r))+
  scale_fill_brewer(palette ="Set3") +
  theme_void()
dev.off()

-->

