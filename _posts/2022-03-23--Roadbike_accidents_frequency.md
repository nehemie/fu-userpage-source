---
layout: post
title: Bike accidents frequency
...


[Yesterday][yesterday], I superposed bike accidents over the
road network. Today, I tried to count the number of accident by
road's geometry.


## Exhibit of the day

A map plot of the road network (geometries) coloured by accidents freq

![map road bike accident frequency][2022-03-23_roads_freq_bike_accidents]

[2022-03-23_roads_freq_bike_accidents]: {{site.canonical}}/assets/exhibit/2022-03-23_roads_freq_bike_accidents.jpg


<figcaption> Data: Strassenverkehrsunfälle nach Unfallort in
Berlin 2020  CC BY Amt für
Statistik Berlin-Brandenburg + Detailnetz Straßenabschnitte CC BY Geoportal Berlin".</figcaption>

Plot made with [R], [sf] and [ggplot2] (code in the source page as comments).

[R]: {{ site.data.links.cran }}
[sf]: {{ site.data.links.cran_sf }}
[ggplot2]: {{ site.data.links.cran_ggplot2 }}
[data]: https://daten.berlin.de/datensaetze/detailnetz-stra%C3%9Fenabschnitte-wfs
[yesterday]: https://userpage.fu-berlin.de/~nehemie/2022-03-22-roads-bike-accidents



<!--

library(sf)
library(dplyr)
library(ggplot2)
library(units)

# Load data road network
# "https://fbinter.stadt-berlin.de/fb/wfs/data/senstadt/s_vms_detailnetz_spatial_gesamt"

dsn <- read_sf("raw_data/Detailnetz-Strassenabschnitte.shp")
dsn$strassenkl <- factor(dsn$strassenkl)
dsn$strassenkl <- factor(dsn$strassenkl,
                            rev(levels(dsn$strassenkl)))
dsn$length <- st_length(dsn)

# Load data: crash
crash <-
  read.csv2("raw_data/AfSBBB_BE_LOR_Strasse_Strassenverkehrsunfaelle_2020_Datensatz.csv",
colClasses = c(rep("character", 3),
               rep("factor", 9),
               rep("integer", 6),
               rep("factor", 1),
               rep("numeric", 5)
))

colnames(crash) |> tolower() ->  colnames(crash)
crash[13:18] <- sapply(crash[13:18] , as.logical)
crash$umonat <- factor(crash$umonat, levels = 1:12)
levels(crash$umonat) <-
     format(seq.Date(as.Date('2000-01-01'), by = 'month', len = 12), "%b")
# Change name and reorder weekday
# 1 = Sonntag 2 = Montag 3 = Dienstag
# 4 = Mittwoch 5 = Donnerstag 6 = Freitag 7 = Samstag
levels(crash$uwochentag) <-
     format(seq.Date(as.Date('2000-01-02'), by = 'day', len = 7), "%a")
crash$uwochentag <- factor(crash$uwochentag,
                           levels(crash$uwochentag)[c(7,1:6)])
# Select Bike Crash
crash <- subset(crash, istrad == TRUE)
crash_longlat <- as.matrix(crash[c("xgcswgs84", "ygcswgs84")])
crash_geom <- sfheaders::sf_point(crash_longlat)
crash <- st_sf(cbind(crash[1:22], geom= st_geometry(crash_geom)))
st_crs(crash) <-  4326
crash <- st_transform(crash, st_crs(dsn))

# Intersect

## Creat 1m buff on crash
cwd <- st_is_within_distance(crash, dsn, set_units(1.5, m))
cwd_df <- cwd %>% unlist %>% table %>% as.data.frame
colnames(cwd_df) <- c("rown", "freq")
dsn_crashed <- dsn[as.integer(as.character(cwd_df$rown)), ]
dsn_crashed$freq  <- cwd_df$freq

# Berlin outline
lor <- st_read("raw_data/lor_planungsraeume_2021.gml")
colnames(lor)[1:6] |> tolower() ->  colnames(lor)[1:6]
lor$plr_id <- lor$plr_id |> as.factor()
subset(lor, select=-bez) -> lor
lor_outer <- st_as_sf(st_union(lor))

ggplot() +
  geom_sf(data = lor_outer,
          fill = "black") +
  geom_sf(data = dsn,
          colour = "white",
          size = 0.3,
         alpha = 0.5) +
  geom_sf(data = dsn_crashed,
          aes(colour = freq),
          size = 1.5,
          alpha = 0.9) +
  theme_void() +
  scale_colour_steps(low = "yellow",
                     high = "red",
                     n.breaks = 8) +
  labs(title = "Frequency of accidents involving a bike",
       subtitle = "By roads in Berlin / 2020",
      caption = "Data: Strassenverkehrsunfälle 2020 + Detailnetz - Straßenabschnitte \nCC BY Amt für Statistik Berlin-Brandenburg & Geoportal Berlin")

ggsave("2022-03-23_roads_freq_bike_accidents.jpg",
       width = 7.0,
       height = 6,
       bg = "white",
       dpi = 220)


-->
