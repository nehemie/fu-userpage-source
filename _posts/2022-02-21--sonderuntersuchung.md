---
layout: post
title: Sonderuntersuchung
---

I usually commute by bike and I regularly enjoy making a 30km
leisure trip to change my mind. Sadly, safety on the road is a
concern when you spend time − short or long − on the saddle.
Accidents involving a bike and a car have often dramatic
consequences. Arriving in a new city such as Berlin requires me
to adapt to a new environment.  The way people drive cars and
ride bikes are different, as well as the infrastructure (and the
weather…). Therefore, to know which situations require special
attention, I wanted to find out what are the main causes of
bike/car accident.

I found that the Federal Statistical Office of Germany publishes
an "[Accident Atlas][aa]". This is a database containing "all"
the accidents involving personal injuries in Germany by year. I
wish it would not be such a huge dataset, but only for the year
2020, there are 237994 records (ca 650 per day!). Each year is
published as a separate file from 2016 to 2020. So there is a lot
of data available to deal with.

Before looking into the raw data for my new residential city, I
saw that the police of Berlin publishes a [general report on the
accident in Berlin][rvf] each year. Moreover it issues a report
dedicated to the accidents involving bike cycling. I was
impressed, and I am wondering how many cities are publishing and
analysing this kind of data.

[aa]: https://unfallatlas.statistikportal.de/
[rvf]: https://www.berlin.de/polizei/aufgaben/verkehrssicherheit/verkehrsunfallstatistik/

## Picture of the day

I love the design from the cover of the special report about bike
accident in Berlin. It contains a lot of useful information.
that I am currently reading.

<figcaption> Source: Cover of the Berlin Bike Accident Report 2020
</figcaption>
![Report's cover][2022-02-21_Radfahrerverkersunfaelle]

[2022-02-21_Radfahrerverkersunfaelle]: {{site.canonical}}/assets/exhibit/2022-02-21_Radfahrerverkersunfaelle.jpg
