---
layout: post
title: Popular Women Names
...

I am a lucky worker of the German capital that recognizes March
8th − International Women's Day − as a public holiday. Berlin is
the first of Germany's 16 federal states and the first
jurisdiction in the European Union to make this day a holiday.

Crashes do not records gender so I took a look at the data
about given name. The city of Berlin provides the distribution of
given names by district ([Liste der häufigen Vornamen
2022][data-vorname]). The data are provided for 2021 in the
form

    given name,count,gender,position

The position refers to the order of the name if a child has
several given names. It is stated that the position designates
the position of in the list of names but that the position
makes "no statement about the main used name". However, for simplicity,
I only kept the name in position one.

## Picture of the day

Most popular "female" name (as indicated in the data) ranked
first in the list.  Plot made with [r]
and [ggplot2]

![Berlin's female given name top 20][2022-03-08-berlin_vorname]

[2022-03-08-berlin_vorname]: {{site.canonical}}/assets/exhibit/2022-03-08_berlin_vorname.jpg

<figcaption> Data: CC BY 3.0 DE "Berliner Landesamt für Bürger-
und Ordnungsangelegenheiten (LABO),
https://github.com/berlinonline/haeufige-vornamen-berlin</figcaption>

[R]: {{ site.data.links.cran }}
[sf]: {{ site.data.links.cran_sf }}
[ggplot2]: {{ site.data.links.cran_ggplot2 }}
[data-vorname]: https://daten.berlin.de/datensaetze/liste-der-haeufigen-vornamen-2021
[lor-note]: 2022-03-03-lebensweltlich
[yesterday_post]: 2022-03-04-lor-and-accident

<!--
library(dplyr)
library(ggplot2)

sets <-
paste0("https://raw.githubusercontent.com/berlinonline/haeufige-vornamen-berlin/master/data/cleaned/2021/",
       c( "charlottenburg-wilmersdorf.csv",
         "friedrichshain-kreuzberg.csv",
         "lichtenberg.csv",
         "marzahn-hellersdorf.csv",
         "mitte.csv",
         "neukoelln.csv",
         "pankow.csv",
         "reinickendorf.csv",
         "spandau.csv",
         "steglitz-zehlendorf.csv",
         "tempelhof-schoeneberg.csv",
         "treptow-koepenick.csv"
         )
       )

 do.call(rbind, lapply(sets, function(path) {
    df <- read.csv(path)
    df[["source"]] <- rep(sub(".csv", "", basename(path)), nrow(df))
    return(df)
} )) -> vorname
vorname$vorname <- as.factor(vorname$vorname)
vorname$source <- as.factor(vorname$source)
vorname$geschlecht <- as.factor(vorname$geschlecht)



vorname %>%
  filter( position == 1) %>%
  group_by(vorname, geschlecht) %>%
  summarise(gesamt = sum(anzahl)) %>%
  filter(gesamt > 100) -> vns


ggplot(data = vns,
             aes(x = reorder(vorname, gesamt),
                 y = gesamt)) +
        geom_col(data = subset(vns, geschlecht == "w")) +
#        geom_col(data = subset(vns, geschlecht == "m"),
#                 aes(y = gesamt*(-1))) +
        labs(title = "Most popular first baby names for women in Berlin",
             subtitle = "Year 2021, Total Birth = 34450, Birth women = 17005 (49.4%)",
             caption = "Data: Liste der häufigen Vornamen 2021 \nCC BY Landesamt für Bürger- und Ordnungsangelegenheiten − Berlin ")+
 xlab("Given name") +
 ylab("Given times (as first ranked name only)") +
        coord_flip() +
        theme_bw()
ggsave("2022-03-08_berlin_vorname.jpg", height = 10, width =6)
-->

