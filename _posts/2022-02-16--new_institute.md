---
layout: post
title: Institut für Wissensgeschichte des Altertums
---

The institute is situated on [Arnimallee 10, 14195
Berlin](https://www.openstreetmap.org/way/103716051) on the
[Dahlem
campus](https://web.archive.org/web/20220217061501/https://www.fu-berlin.de/en/redaktion/orientierung/dahlem/index.html)
of the Freie Universität Berlin. The name of the street comes
from Bernd von Arnim-Criewen (1850–1939), who lived in the street
when he was a Prussian Minister of Agriculture (thank you
[wikipedians](https://de.wikipedia.org/w/index.php?title=Arnimallee&oldid=212887612))

## Picture of the day

There is a nice garden behind the institute, or just in front of
my window (on the bottom right of the picture). It is a spacious,
green space that should be a beautiful place to spend some time
with colleagues from spring to autumn (today it was a cloudy and drizzly).

<figcaption> CC BY-SA 4.0 Néhémie Strupler (2022-02-16)</figcaption>
![Picture of the backyard of the Institute für Wissensgeschichte
des Altertums at Freie Universität Berlin][2022-02-16_institute]

[2022-02-16_institute]: {{site.canonical}}/assets/exhibit/2022-02-16_institute.jpg

