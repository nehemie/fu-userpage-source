---
layout: post
title: Age of buildings in Berlin
---

This week, I was looking into the open data offered by the
municipality of Berlin (see <https://daten.berlin.de/>). When I
was scrolling through the entries, one data set caught my
attention. It is a classification of Berlin’s neighborhood
according to the [age of the building][daten_building] (available
in the GML format [here][gml_buildings]). It picked my curiosity
because it was a dream to obtain a similar classification for my
PhD. I also dated houses from a capital city, but is was Hattuša
and housed constructed and destroyed between 1800 and 1200 BCE.
The main difference is my classification was done with interval
classes of 100 years instead of 10 years!


[daten_building]: https://daten.berlin.de/datensaetze/geb%C3%A4udealter-der-wohnbebauung-umweltatlas-wfs
[gml_buildings]: https://fbinter.stadt-berlin.de/fb/wfs/data/senstadt/s06_12baualter?service=wfs&version=2.0.0&request=GetFeature&srsName=EPSG%3A25833&TYPENAMES=s06_12baualter


## Picture of the day

A quick map of Berlin’s neighborhood coloured according to the
age of the buildings. The map was done with [r], [sf] and [ggplot2] (the R code is
available as comment in the source code of this page).

[R]: {{ site.data.links.cran }}
[sf]: {{ site.data.links.cran_sf }}
[ggplot2]: {{ site.data.links.cran_ggplot2 }}

I do not know enough of Berlin to be able to analyse the
distribution. My guess is that part of Berlin's history can be
seen. For example, buildings with a construction date later than
1990 are mainly located in the east,

<!--

library(sf)
geb <- read_sf("s06_12baualter.gml")

# Order levy by dates
geb$ueberw_dekade_woh_neu <-
  geb$ueberw_dekade_woh_neu |>
   as.factor() |>
   relevel(ref = c("bis 1900")) |>
relevel(ref = c("gemischte Baualtersklasse"))
# Colour palette
rygb <- c("red", "green", "blue")
color.palette <- colorRampPalette(rygb)
color_palette <- append("black", color.palette(12), after= 1)
color_palette <- append("yellow", color_palette, after= 12)

library(ggplot2)
jpeg("age_building_ber.jpg", width=800, quality=90)
ggplot(geb) +
  geom_sf(aes(fill=ueberw_dekade_woh_neu), colour = "transparent" ) +
   guides(fill=guide_legend("Age Class")) +
  theme_minimal() +
   scale_fill_manual(values = color_palette)
dev.off()


-->
<figcaption> Datenlizenz: Deutschland – Namensnennung – Version 2.0,
Senatsverwaltung für Stadtentwicklung und Wohnen Berlin
</figcaption>

![Map of the age of Berlin Buildings][2022-02-18_buildings_age]

[2022-02-18_buildings_age]: {{site.canonical}}/assets/exhibit/2022-02-18_buildings_age.jpg

