---
layout: post
title: Time Travel
---

Sifting through the report from the Berlin Police about bike
accidents (see my [yesterday post][yesterday_post]), there are
some remarks that I never considered consciously. I usually try
to avoid busy hours when I am using a bike. I find it less
stressful and I can relax accordingly. When I started to
regularly spend time on the bike, I was living in Istanbul, and I
did most of my leisure rides early in the morning (before 7.30
am) to be at home when the city gets congested. I probably
implicitly felt that it was safer, but I did not know about it.

[yesterday_post]: 2022-02-21-sonderuntersuchung

The "Sonderuntersuchung" about bike accidents in Berlin addresses
the time of the accidents (p. 2 from the report 2020).
The report highlights that the number of bike traffic accidents
depends on the season (more accidents occur during spring and
summer when more people are cycling), and the time when accidents
happens matches the overall pattern of road accidents (car or
motorcycle accident). My guess is that it is (obviously) highly
correlated with traffic intensity, but I did not see some number
about traffic intensity in the first pages of the report.

## Picture of the day

This is a graph of the hour of bike crashes split by
the day of the week for Berlin and Brandenburg (made with R; code is provided as a comment of this webpage).

<!--
library(sf)

# Select Berlin and Brandenburg
# ULAND = 11 (Berlin) | 12 (Brandenburg)
berbrand <- read.csv2(pipe("awk 'BEGIN {FS=\";\"} {if ($3 == 11 || $3 == 12 ) print $0}' Unfallorte2020_LinRef.csv"),
header=FALSE,
colClasses = c(rep("character", 2),
               rep("factor", 18),
               rep("character", 4),
               rep("factor", 1)
))
readLines("Unfallorte2020_LinRef.csv", n=1) |>
  strsplit(";") |> unlist()  |> tolower() -> colnames(berbrand)
colnames(berbrand)
levels(berbrand$uwochentag) <- c("So", "Mo", "Di", "Mi", "Do",
                                 "Fr", "Sa")

berbrand$uwochentag <-
  factor(berbrand$uwochentag,levels(berbrand$uwochentag)[c(2:7,1)])

# Colour palette
rygb <- c("blue", "yellow", "red", "yellow", "blue")
color.palette <- colorRampPalette(rygb)
color_palette <- color.palette(24)

head(berbrand)
library(ggplot2)
library(dplyr)

jpeg("bike_unfaelle_stunde.jpg", width=800, quality=90)
ggplot(berbrand, aes(x = uwochentag, group=ustunde)) +
  geom_bar(stat = "count",
           aes(fill = ustunde), position = "dodge") +
  stat_count(geom = "text",
             aes(label = ustunde),
             position=position_dodge2(1),
             colour = "black", size = 2.5, vjust=1) +
                      theme_minimal()+ guides(fill="none") +
                      scale_fill_manual(values = color_palette) +
                      labs(x = "Wochentag")
dev.off()
-->
<figcaption>
Data Copyright: DL-DE BY 2.0 Statistische Ämter des Bundes und der Länder, Deutschland, 2021.
</figcaption>

![Timy by day of accident with bike][2022-02-22_bike_unfaelle_stunde]

[2022-02-22_bike_unfaelle_stunde]: {{site.canonical}}/assets/exhibit/2022-02-22_bike_unfaelle_stunde.jpg

Overall, during the week, accidents occur between 7am and 9pm and
the peak is situated between  3pm  and 6pm.  Each
weekday has almost the same traffic accident rate, but also a
similar pattern. In contrast, the weekend records fewer cases, but
more during the night.


[dldeby]: {{ site.data.links.dlde_by }}
