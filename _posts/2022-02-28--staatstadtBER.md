---
layout: post
title: Land und Stadt zugleich
...

Before starting to analyse some geographical data from Berlin,
especially official statistics, I have to understand how the city
is organised. The data are often provided with a relation to an
administrative district. Knowing what these districts
represent may help to avoid wrong inferences.

I was a bit disappointed with the website <https://berlin.de/>.
It is difficult to find information about the
administration of the city. The [Berlin article][wiki-berlin]
from Wikipedia offers a better entry to explore the organisation
of the city. However, some fundamental information can be gleaned
from the [The Constitution of Berlin][berlin-constitution].  It
was consented by the people of Berlin in the referendum of 22
October 1995.

[berlin-constitution]: https://www.berlin.de/rbmskzl/en/the-governing-mayor/the-constitution-of-berlin/

At the beginning, paragraph 1, article 1, section 1 ("Fundamental
Provisions"),  it states: "Berlin is both a German Land and a
city." (in original [*Berlin ist ein deutsches Land und zugleich
eine Stadt.*][ber-verfass]


[wiki-berlin]: https://de.wikipedia.org/wiki/Berlin
[ber-verfass]: (https://www.berlin.de/rbmskzl/regierende-buergermeisterin/verfassung/)


## Picture of the day

From 2008 to 2020 "be Berlin" was the city’s motto brand. Since
September 12th 2020, the citystate Berlin has a new brand identity
"[wir.berlin][wir-berlin]". 

<figcaption> The #WIRSINDEINBERLIN-Generator's starting example
from the official website</figcaption> ![Example of the generator
from 2022-02-28][2022-02-28-beispielstart]

[2022-02-28-beispielstart]: {{site.canonical}}/assets/exhibit/2022-02-28-beispielstartN.png


[wir-berlin]: https://wir.berlin/
