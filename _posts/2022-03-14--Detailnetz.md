---
layout: post
title: Detailnetz
...


The "Detailnetz" provides Berlin's main road network ([data][data]). Two variables gives important 
information about the roads:

 - *strassenkl*: class Road classification:
   + I large-scale road connection
   + II superordinate road connection
   + III local road connection
   + IV supplementary road
   + V not StEP
   + 0 other section
 - verkehrsri: Traffic direction
   + R = in digitization direction
   + G = in opposite direction
   + B = both traffic directions possible

## Exhibit of the day

A crude map of the main road system in Berlin

![Berlin's map of main road network][2022-03-14_detailnetz]

[2022-03-14_detailnetz]: {{site.canonical}}/assets/exhibit/2022-03-14_detailnetz.jpg

<figcaption> Data: CC BY "Geoportal Berlin / Detailnetz Straßenabschnitte".</figcaption>

Plot made with [R], [sf] and [ggplot2].


[R]: {{ site.data.links.cran }}
[sf]: {{ site.data.links.cran_sf }}
[ggplot2]: {{ site.data.links.cran_ggplot2 }}
[data]: https://daten.berlin.de/datensaetze/detailnetz-stra%C3%9Fenabschnitte-wfs


<!--
library(sf)
library(dplyr)
library(ggplot2)

# "https://fbinter.stadt-berlin.de/fb/wfs/data/senstadt/s_vms_detailnetz_spatial_gesamt"
# https://tsb-opendata.s3.eu-central-1.amazonaws.com/detailnetz_strassenabschnitte/Detailnetz-Strassenabschnitte.gml
dsn <- read_sf("raw_data/Detailnetz-Strassenabschnitte.gml")
lor <- st_read("raw_data/lor_planungsraeume_2021.gml")
colnames(lor)[1:6] |> tolower() ->  colnames(lor)[1:6]
lor$plr_id <- lor$plr_id |> as.factor()
subset(lor, select=-bez) -> lor
lor_outer <- st_as_sf(st_union(lor))


ggplot(dsn) +
  geom_sf(data=lor_outer) +
  geom_sf(lwd=0.5) +
  theme_void() +
  theme(legend.key.width = unit(0.1, "npc"),
        legend.position="bottom") +
  labs(title = "Road network",
       subtitle = "Main axes in Berlin",
            caption = "Data: Detailnetz - Straßenabschnitte \nCC BY Geoportal Berlin")
ggsave("2022-03-14_detailnetz.jpg", width=7.0,
       height=6, bg="white", dpi = 160)

-->

