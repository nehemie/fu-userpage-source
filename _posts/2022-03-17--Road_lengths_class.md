---
layout: post
title: Road Length by class
...

[Yesterday][yesterday] we took a look at the length of each network
by class. Today, let us see the variation of the length of roads
by class


## Exhibit of the day

A box plot of the length of road according to the class

![Boxplot length road][2022-03-17_road_length_class]

[2022-03-17_road_length_class]: {{site.canonical}}/assets/exhibit/2022-03-17_roadlength_by_class.jpg


<figcaption> Data: CC BY "Geoportal Berlin / Detailnetz Straßenabschnitte".</figcaption>

Plot made with [R] and [ggplot2] (code in the source page as comments).

[R]: {{ site.data.links.cran }}
[sf]: {{ site.data.links.cran_sf }}
[ggplot2]: {{ site.data.links.cran_ggplot2 }}
[data]: https://daten.berlin.de/datensaetze/detailnetz-stra%C3%9Fenabschnitte-wfs
[yesterday]: https://userpage.fu-berlin.de/~nehemie/2022-03-16-road-lengths



<!--
library(sf)
library(dplyr)
library(ggplot2)
library(units)

# "https://fbinter.stadt-berlin.de/fb/wfs/data/senstadt/s_vms_detailnetz_spatial_gesamt"
# https://tsb-opendata.s3.eu-central-1.amazonaws.com/detailnetz_strassenabschnitte/Detailnetz-Strassenabschnitte.gml
dsn <- read_sf("raw_data/Detailnetz-Strassenabschnitte.shp")
dsn$strassenkl <- factor(dsn$strassenkl)
dsn$strassenkl <- factor(dsn$strassenkl,
                            rev(levels(dsn$strassenkl)))
dsn$length <- st_length(dsn)

dsn_kl_length <- dsn %>% as.data.frame %>%
  group_by(strassenkl) %>%
  summarise(length = sum(length)) %>%
  mutate(length = set_units(length,km))

dsn %>%
#  filter(length < set_units(5000, m)) %>%
  ggplot() +
  geom_boxplot(aes(x = length, y = strassenkl, fill=strassenkl)) +
  scale_fill_brewer(palette="OrRd",
                    limits = rev(levels(dsn$strassenkl)),
                    direction = -1) +
  theme_minimal() +
  coord_flip() +
  theme(legend.position="bottom") +
  labs(colour = "Road class") +
  labs(title = "Length of road by classes",
       subtitle = "Main axes in Berlin",
       caption = "Data: Detailnetz - Straßenabschnitte \nCC BY
       Geoportal Berlin") 
ggsave("2022-03-17_roadlength_by_class.jpg",
       width=7.0,
       height=9,
       bg="white",
       dpi = 160)
-->

