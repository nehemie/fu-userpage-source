---
layout: post
title: Lengthy Road
...


[Last post][last_post] was about the length of road when you
merge the different geometries from the [data set][data] together.

There is still an outlier in the data set, but this time it is
45km long. It is the "A 100 BAB Stadtring", BAB meaning
"*Bundesautobahn*", that translates in "Federal Motorway 100".
There a [Wikipedia
article](https://en.wikipedia.org/wiki/Bundesautobahn_100) about
it.


## Exhibit of the day

A map of the 4 lengthiest roads

![map lengthy road][2022-03-21_lengthy_roads]

[2022-03-21_lengthy_roads]: {{site.canonical}}/assets/exhibit/2022-03-21_lengthy_roads.jpg


<figcaption> Data: CC BY "Geoportal Berlin / Detailnetz Straßenabschnitte".</figcaption>

Plot made with [R], [sf] and [ggplot2] (code in the source page as comments).

[R]: {{ site.data.links.cran }}
[sf]: {{ site.data.links.cran_sf }}
[ggplot2]: {{ site.data.links.cran_ggplot2 }}
[data]: https://daten.berlin.de/datensaetze/detailnetz-stra%C3%9Fenabschnitte-wfs
[last_post]: https://userpage.fu-berlin.de/~nehemie/2022-03-18-road-lengths-outlier


Two things surprised me. First it is the class of the A100. After
researching, I found that some parts of the A100 are classed as
"I" and other part as classed as "II". The other thing that I
found surprising, is that the A100 is twice so long as the other
3 highways, but this not seems to be the case on the map. I think
this is a result of the digitisation as some parts of the road are
drawn separately depending on the direction, making it longer.

Take away: It is not easy to analyse this dataset!

<!--
library(sf)
library(dplyr)
library(ggplot2)
library(units)

# "https://fbinter.stadt-berlin.de/fb/wfs/data/senstadt/s_vms_detailnetz_spatial_gesamt"
# https://tsb-opendata.s3.eu-central-1.amazonaws.com/detailnetz_strassenabschnitte/Detailnetz-Strassenabschnitte.gml
dsn <- read_sf("raw_data/Detailnetz-Strassenabschnitte.shp")
dsn$strassenkl <- factor(dsn$strassenkl)
dsn$strassenkl <- factor(dsn$strassenkl,
                            rev(levels(dsn$strassenkl)))
dsn$length <- st_length(dsn)

dsn %>% filter(strassenna == "Havelchaussee")
dsn_data <-  dsn
st_geometry(dsn_data) <- NULL
dsn_data  %>%
  group_by(strassensc) %>%
  mutate(laenge = sum(laenge)) -> dsn_sl

dsn_sl$laenge  %>%  set_units(m) -> dsn_sl$laenge
dsn_sl$laenge  %>%  set_units(km) -> dsn_sl$laenge

dsn_sl %>%
  distinct(strassensc, .keep_all = TRUE) %>%
  arrange(desc(laenge)) %>%
  select(strassenna, laenge, strassenkl)


dsn_sl %>%
  distinct(strassensc, .keep_all = TRUE) %>%
  arrange(desc(laenge)) %>%
  select(strassensc) %>%
  .[c(1:4),] -> t10

dsn_t10 <- dsn[dsn$strassensc %in% t10$strassensc, ]

ggplot(dsn_t10) +
  geom_sf(data=lor_outer, fill = "black") +
  geom_sf(aes(colour = strassenna),
          lwd = 0.5) +
#  facet_wrap(vars(strassenna))+
  theme_void() +
  theme(legend.position="right",
        legend.key = element_rect(fill = "black", colour =
                                  "white")) +
  labs(colour = "Road name") +
  scale_colour_brewer(palette="OrRd") +
  labs(title = "Road network",
       subtitle = "4 longest roads in Berlin",
       caption = "Data: Detailnetz - Straßenabschnitte \nCC BY Geoportal Berlin")

ggsave("2022-03-21_lengthy_roads.jpg",
       width=10,
       height=6,
       bg="white",
       dpi = 160)
-->

