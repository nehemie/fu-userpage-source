---
layout: default
title: "Welcome!"
type: home
---

# Welcome to my Userpage

---

Hello, I am Néhémie, a
[PostDoc](https://www.geschkult.fu-berlin.de/e/wissensgeschichte/mitarbeiter_innen/index.html)
at the [Institute for the History of Knowledge in
Antiquity](https://www.geschkult.fu-berlin.de/e/wissensgeschichte/institutsportrait/index.html)
(Freie Universität Berlin). I use the userpage environment
provided by [ZEDAT](https://www.zedat.fu-berlin.de/Home) mainly for testing purpose and to post some
random notes.

---

<div class="intro-warranty">

Permission is hereby granted, free of charge without restriction,
the rights to read, agree, disagree, laugh, distribute, link,
like, enjoy, ignore and/or sell (good luck!) copies of this
website provided attribution to the original author and website.

The website is provided "as is", without warranty of fitness for
a particular purpose.
</div>


---


<footer class=my_contact>

<h2> Contact </h2>


{% include my_contact.html %}

</footer>


## Notes

{% for post in site.posts reversed %}
{{ post.date | date_to_string }} &raquo; [{{ post.title }}]({{ post.url | remove_first: "/" }})
{% endfor %}

---

