#!/usr/bin/perl -w

sub writeCgiEntry {
    my($logFile) = "cgi.log";
    my($script)  = __FILE__;
    my($name)    = $ENV{'REMOTE_HOST'};
    my($addr)    = $ENV{'REMOTE_ADDR'};
    my($browser) = $ENV{'HTTP_USER_AGENT'};
    my($time)    = time;


    open(LOGFILE,">>$logFile") or die("Can't open cgi log file.\n");
    print LOGFILE ("$script!$name!$addr!$browser!$time\n");
    close(LOGFILE);
}


writeCgiEntry();


# do some CGI activity here.


print "Content-type: text/html\n\n";
print "<HTML>";
print "<TITLE>CGI Test</TITLE>";
print "<BODY><H1>Testing!</H1></BODY>";
print "</HTML>";
